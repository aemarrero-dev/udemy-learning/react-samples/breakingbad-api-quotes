import React, {useState, useEffect} from 'react';
import styled from '@emotion/styled'; //La instalamos con npm i @emotion/core @emotion/styled
import Frase from './components/Frase';

//Styled component div
const Contenedor = styled.div`
  display: flex;
  align-items: center;
  padding-top: 5rem;
  flex-direction: column;
`;

//Styled Component Boton
const Boton = styled.button`
  background: -webkit-linear-gradient(top left, #007d35 0%, #007d35 40%, #0f574e 100%);
  background-size: 300px;
  font-family: Arial, Helvetica, sans-serif;
  color: #fff;
  margin-top: 3rem;
  padding: 1rem 3rem;
  font-size: 2rem;
  border: 2px solid black;
  transition: background-size .8s ease;

  :hover {
    cursor: pointer;
    background-size: 400px;
  }
`;

function App() {

  //State para guardar frase obtenida
  const [frase, guardarFrase] = useState({});


  /*//Consulto la API con FETCH
  const consultarAPI = () => {
    const api = fetch('https://breaking-bad-quotes.herokuapp.com/v1/quotes');
    const frase = api.then(respuesta => respuesta.json() );
    frase.then(resultado => console.log(resultado));
  }*/

  //Consulto la API con ASYNC. Detiene la ejecucion del codigo hasta que se complete el fetch y luego retorna.
  const consultarAPI = async () => {
    const api = await fetch('https://breaking-bad-quotes.herokuapp.com/v1/quotes');
    const frase = await api.json();
    guardarFrase(frase[0]); //Guardamos la frase en el State.
  }

  //Carga una frase cuando este componente este listo
  useEffect( () => {
    consultarAPI()
  }, []);

  return (
    <Contenedor>
      <Frase
        frase = {frase}
      />
      <Boton
        onClick = {consultarAPI} //espera clic y ejecuta funcion
        //onClick = {() => consultarAPI()} //espera clic y ejecuta funcion
        //onClick = {consultarAPI()} //ejecuta funcion sin esperar clic
      >
        Obtener Frase
      </Boton>
    </Contenedor>
  );
}

export default App;


/* 
3 FORMAS COMUNES DE CONSULTAR API´S CON REACT:
1. FETCH API Y AJAX (NATIVO DE JAVASCRIPT)
2. AXIOS
3. JQUERY AJAX
*/